#Before Starting
Design patterns used
Singleton: By default, all the Spring beans are singleton, as I am not creating a shopping cart or something more sophisticated it's ok to use singleton
MVC: The model will be the Employee entity, Controller will be EmployeeRestController and View the JSON Response
FrontController: By default, Spring uses a front controller inside it when you make the petition to EmployeeRestController
DAO: EmployeeRepository will act as a DAO with the common methods for a CRUD, also GenericService will act as one to have the common CRUD behavior

#Things used
 Spring boot  2.1.7 - Spring Framework 5.1
 Modules: spring-boot-starter-data-jpa
          spring-boot-starter-web
		  spring-boot-starter-test
		  spring-boot-maven-plugin

  Swagger 2 - springfox UI
  Lombok
		  
#If you want to continue modifing this
This is a Maven project so you could use STS or any ide you want in my case I use STS
just go to import>maven project>select the root folder and there you go


#Intallation Instructions

Download the application/json
Enter to a CDM (you have to have Maven installed if you don't have it see https://docs.wso2.com/display/IS323/Installing+Apache+Maven+on+Windows)
once in the cmd locate the folder where the POM file is and execute
mvn clean package

for example, in my CMD I am in the folder employee and I executed the command mvn clean package

C:\Users\Yussef_Gonzalez\Desktop\Work\_Pivotal\employee>mvn clean package

it will create a jar file called employee-0.0.1-SNAPSHOT.jar inside the target folder
ex: C:\Users\Yussef_Gonzalez\Desktop\Work\_Pivotal\employee\target
change to that folder using your CMD
ex:  C:\Users\Yussef_Gonzalez\Desktop\Work\_Pivotal\employee>cd C:\Users\Yussef_Gonzalez\Desktop\Work\_Pivotal\employee\target

once there execute:  java -jar employee-0.0.1-SNAPSHOT.jar
example: C:\Users\Yussef_Gonzalez\Desktop\Work\_Pivotal\employee\target>java -jar employee-0.0.1-SNAPSHOT.jar

You should see something like:

C:\Users\Yussef_Gonzalez\Desktop\Work\_Pivotal\employee\target>java -jar employee-0.0.1-SNAPSHOT.jar

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.7.RELEASE)

2019-08-15 16:09:07.790  INFO 19044 --- [           main] com.kenzan.employee.EmployeeApplication  : Starting EmployeeApplication v0.0.1-SNAPSHOT on EPMXGUAW0268 with PID 19044 (C:\Users\Yussef_Gonzalez\Desktop\Work\_Pivotal\employee\target\employee-0.0.1-SNAPSHOT.jar started by Yussef_Gonzalez in C:\Users\Yussef_Gonzalez\Desktop\Work\_Pivotal\employee\target)
2019-08-15 16:09:07.795  INFO 19044 --- [           main] com.kenzan.employee.EmployeeApplication  : No active profile set, falling back to default profiles: default

--------------------------

There you got :) if you want to stop the app press CTRL+C inside the CMD ... otherwise, continue with the tutorial

-------------

In order to test the service using CURL

1.- open a CMD window that allows you use the command CURL (I'll use Git bash)
2.- Execute the commands :
   A) for get all Employees:
        curl -v localhost:8081/employee | json_pp

   B) for get an Employee by id :
        curl -v localhost:8081/employee/1 | json_pp

   C)  for delete an employee by id:
        curl -X DELETE localhost:8081/employee/1 | json_pp
		
   F) for update an employee by id:
       curl -X PUT localhost:8081/employee/1 -H 'Content-type:application/json' -d '{"id": "1", "firstName":"Yussef", "middleInitial":"Gonzalez", "lastName":"Vargas", "dateOfBirth":"'1999-01-30'",  "dateOfEmployment":"2018-01-30",  "status":"ACTIVE"}'
	   
   G) for insert a new employee:
       curl -X POST localhost:8081/employee -H 'Content-type:application/json' -d '{"id": "100", "firstName":"New User", "middleInitial":"Test", "lastName":"A", "dateOfBirth":"'1999-01-30'",  "dateOfEmployment":"2018-01-30",  "status":"ACTIVE"}'
	   
	   
	   
	   
3.- if you want to use a GUI app you can use Swagger UI
     enter the url: http://localhost:8081/swagger-ui.html
	 and you will get all the services and some examples there too
	 
4.- if you want to validate inside the database the changes enter to:
     http://localhost:8081/h2-console
	 validate that the parammeters are like these:
	 Saved Settings:Generic H2 (Embedded)
	 Setting Name:Generic H2 (Embedded)
	 Driver Class: 	org.h2.Driver
	 JDBC URL:jdbc:h2:mem:testdb
	 User Name:sa
	 Password: {{this should be blank}}
	 
	 and just click on the "Connect" button 
	 
	 you could run the query:  SELECT * FROM EMPLOYEE 
	 
	   

	   
	   
