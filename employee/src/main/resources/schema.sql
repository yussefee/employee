create table EMPLOYEE
(
   id integer(25) not null,
   firstName  varchar(75) not null,
   middleInitial  varchar(75) not null,
   lastName   varchar(75) not null,
   dateOfBirth DATE not null,
   dateOfEmployment  DATE not null,
   status  varchar(20) not null,
   primary key(id)
);

