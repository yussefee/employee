package com.kenzan.employee.rc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.kenzan.employee.entity.Employee;
import com.kenzan.employee.exception.EmployeeDuplicatedException;
import com.kenzan.employee.exception.EmployeeNotFoundException;
import com.kenzan.employee.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeRestController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping(produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<List<Employee>> getAll() {
		List<Employee> activeEmployees = employeeService.getAll();
		return new ResponseEntity<>(activeEmployees, HttpStatus.OK);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<Employee> saveEmployee(@Valid @RequestBody Employee newEmployee) {
		Employee employee = null;
		try {
			employee = employeeService.save(newEmployee);
		} catch (EmployeeDuplicatedException exception) {
			throw new ResponseStatusException(HttpStatus.FOUND, exception.getMessage(), null);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<Employee> getById(@PathVariable Integer id) {
		Employee employee = null;
		try {
			employee = employeeService.getById(id);
		} catch (EmployeeNotFoundException exception) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), null);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@PutMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<Employee> updateEmployee(@Valid  @RequestBody Employee newEmployee, @PathVariable Integer id) {
		Employee employee = null;
		try {
			employee = employeeService.update(newEmployee);
		} catch (EmployeeNotFoundException exception) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), null);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<Employee> deleteEmployee(@PathVariable Integer id) {
		Employee employee = null;
		try {
			employee = employeeService.delete(id);
		} catch (EmployeeNotFoundException exception) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), null);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}
	
	
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}

}
