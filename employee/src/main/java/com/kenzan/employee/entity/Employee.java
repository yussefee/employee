package com.kenzan.employee.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Employee implements Serializable {

	private static final long serialVersionUID = 2157317464300783379L;

	@Id
	@Column
	@Getter
	@Setter
	@NotNull
	private Integer id;

	@Column
	@Getter
	@Setter
	@NotNull
	private String firstName;

	@Column
	@Getter
	@Setter
	@NotNull
	private String middleInitial;

	@Column
	@Getter
	@Setter
	@NotNull
	private String lastName;

	@Column
	@Getter
	@Setter
	@NotNull
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date dateOfBirth;

	@Column
	@Getter
	@Setter
	@NotNull
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date dateOfEmployment;

	@Column
	@Getter
	@Setter
	@NotNull
	private String status;

}
