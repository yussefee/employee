package com.kenzan.employee.exception;

public class EmployeeNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3660643920789245175L;
	
	public EmployeeNotFoundException(String message){
		super(message);
	}
	
	

}
