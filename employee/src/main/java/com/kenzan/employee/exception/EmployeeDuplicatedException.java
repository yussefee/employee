package com.kenzan.employee.exception;

public class EmployeeDuplicatedException extends RuntimeException {

	private static final long serialVersionUID = -3660643920789245175L;
	
	public EmployeeDuplicatedException(String message){
		super(message);
	}
	
	

}
