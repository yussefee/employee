package com.kenzan.employee.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kenzan.employee.constants.Status;
import com.kenzan.employee.entity.Employee;
import com.kenzan.employee.exception.EmployeeDuplicatedException;
import com.kenzan.employee.exception.EmployeeNotFoundException;
import com.kenzan.employee.repository.EmployeeRepository;
import com.kenzan.employee.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public List<Employee> getAll() {
		List<Employee> employees = employeeRepository.findAll();
		employees = removeInactiveEmployees(employees);
		return employees;
	}


	@Override
	public Employee getById(Integer id) {
		Optional<Employee> employee = getEmployeeById(id);
		wasEmployeeNotFound(employee);
		noInactiveEmployeeAllowed(employee);
		return employee.get();
	}

	@Override
	public Employee save(Employee t) throws EmployeeDuplicatedException {
		Optional<Employee> employee =  getEmployeeById(t.getId());
		isEmployeeAlreadyInDB(employee);
		return employee.orElseGet(() -> employeeRepository.save(t));
	}

	@Override
	public Employee delete(Integer id) {
		Optional<Employee> employee =  getEmployeeById(id);
		wasEmployeeNotFound(employee);
		Employee employeeInactiveStatus = employee.get();
		employeeInactiveStatus.setStatus(Status.INACTIVE.getStatus());
		return employeeRepository.save(employeeInactiveStatus);
	}

	@Override
	public Employee update(Employee t) {
		Optional<Employee> employee =  getEmployeeById(t.getId());
		wasEmployeeNotFound(employee);
		return employeeRepository.save(t);
	}
	
	
	private void wasEmployeeNotFound(Optional<Employee> employee) {
		if(!employee.isPresent()) {
			throw new EmployeeNotFoundException("Employee not found");
		} 
	}

	private void isEmployeeAlreadyInDB(Optional<Employee> employee) {
		if(employee.isPresent()) {
			throw new EmployeeNotFoundException("Employee already exist");
		}
	}
	
	private  Optional<Employee> getEmployeeById(Integer id) {
		return employeeRepository.findById(id);
	}


	private void noInactiveEmployeeAllowed(Optional<Employee> employee) {
		if(isInactiveEmployee(employee)) {
			wasEmployeeNotFound( Optional.empty());
		}
	}

	private boolean isInactiveEmployee(Optional<Employee> employee) {
		return employee.get().getStatus().equals(Status.INACTIVE.getStatus());
	}
	
	private List<Employee> removeInactiveEmployees(List<Employee> employees){
		return employees.parallelStream()
		.filter(employee -> employee.getStatus().equals(Status.ACTIVE.getStatus()))
		.collect(Collectors.toList());
	}

}
