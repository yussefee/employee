package com.kenzan.employee.service;

import java.util.List;

public interface GenericService<T,ID> {

	List<T> getAll();

	T getById(ID id);

	T save(T t);
	
	T update(T t);

	T delete(ID id);

}
