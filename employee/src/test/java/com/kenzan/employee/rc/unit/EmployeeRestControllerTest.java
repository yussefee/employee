package com.kenzan.employee.rc.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.kenzan.employee.constants.Status;
import com.kenzan.employee.entity.Employee;
import com.kenzan.employee.rc.EmployeeRestController;
import com.kenzan.employee.service.EmployeeService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = EmployeeRestController.class, secure = false)
public class EmployeeRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private EmployeeService employeeService;

	private static List<Employee> employeesList;

	@BeforeClass
	public static void setupTestData() {
		employeesList = new ArrayList<Employee>();

		Employee employeeOne = new Employee();
		employeeOne.setId(0);
		employeeOne.setFirstName("Yussef");
		employeeOne.setMiddleInitial("Gonzalez");
		employeeOne.setLastName("Vargas");
		employeeOne.setStatus(Status.ACTIVE.getStatus());
		employeeOne.setDateOfBirth(new Date());
		employeeOne.setDateOfEmployment(new Date());

		employeesList.add(employeeOne);

	}

	@Test
	public void getAllEmployees200OkStatusCode() throws Exception {
		String apiUrl = "/employee";
		// Setup "Mockito" to mock employeeService
		Mockito.when(employeeService.getAll()).thenReturn(employeesList);
		// Build a GET Request and send it to the test server
		RequestBuilder rb = MockMvcRequestBuilders.get(apiUrl).accept(MediaType.APPLICATION_JSON);
		MvcResult r = mockMvc.perform(rb).andReturn(); // throws Exception
		// Validate status code
		int statusCode = r.getResponse().getStatus();
		assertEquals(statusCode, HttpStatus.OK.value());
	}

	@Test
	public void getAllEmployeesThatAreJustActive() throws Exception {
		String apiUrl = "/employee";
		// Setup "Mockito" to mock employeeService
		Mockito.when(employeeService.getAll()).thenReturn(employeesList);
		// Build a GET Request and send it to the test server
		RequestBuilder rb = MockMvcRequestBuilders.get(apiUrl).accept(MediaType.APPLICATION_JSON);
		MvcResult r = mockMvc.perform(rb).andReturn(); // throws Exception
		String response = r.getResponse().getContentAsString();
		assertThat(!response.contains(Status.INACTIVE.getStatus()));
	}

	}


